package com.demo.demoproject.steps.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.demo.demoproject.steps.entities.Task;

@RestController
@RequestMapping("/tasks")
public class TaskS1Controller {

	private final List<Task> taskList = new ArrayList<>();

    @GetMapping("")
	public ResponseEntity<List<Task>> fetchAllTask(){
		return ResponseEntity.ok().body(taskList);
	}

	@PostMapping("") //agregar un nuevo registro
	public ResponseEntity <Task> postTask(@RequestBody Task newTask){
		newTask.setActive(false);
		newTask.setCreatedAt(LocalDateTime.now());
		long id= taskList.size() +1; //le ponemos un id al objeto
		newTask.setId(id);
		taskList.add(newTask); //lo agregamos a la lista

		return ResponseEntity.status(HttpStatus.NO_CONTENT).body(newTask);
	}

	@DeleteMapping("/{taskId}")
	public ResponseEntity<?> deleteTask(@PathVariable("taskId") Long taskId){
		return taskList
					.stream()	
					.filter(current -> taskId == current.getId())
					.findFirst()
					.map( task -> {
						taskList.remove(task);
						return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
					})
					.orElse(ResponseEntity.status(HttpStatus.NOT_FOUND).build());
		
		//return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	}

	@PutMapping("/update")
	public ResponseEntity<String> putTask(){
		return ResponseEntity.ok().body("update");
	}
    

	@GetMapping("/{taskId}")//indica que vamos a enviar una variable, debe ser igual que el parametro de la funcion
	public ResponseEntity<Task> fetchTaskById(@PathVariable("taskId") Long taskId){ //retornar solo un registro por Id
		//buscamos por id
		return taskList
				.stream()
				.filter(current -> taskId == current.getId()) //por cada elemento que haya en la lista evalua si el id es igual
				.findFirst() //obtenemos el primero de la nueva lista, se supone solo hay uno
				.map(task -> ResponseEntity.ok().body(task)) //es una forma de decir si este objeto es distinto de null
				.orElse(ResponseEntity.status(HttpStatus.NOT_FOUND).build());//si no hay un elemento que coincida responde con un status de not found
		
		//return ResponseEntity.ok().body(taskList);
	}
}
